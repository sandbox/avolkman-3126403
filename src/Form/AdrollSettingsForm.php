<?php

namespace Drupal\adroll\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class AdrollSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'adroll_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'adroll.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('adroll.settings');

    $form['adroll_adv_id'] = [
      '#type' => 'textfield',
      '#title' => t('Advertiser ID'),
      '#default_value' => $config->get('adroll_adv_id'),
      '#size' => 30,
      '#maxlength' => 30,
      '#required' => TRUE,
    ];
    $form['adroll_pix_id'] = [
      '#type' => 'textfield',
      '#title' => t('Pixel ID'),
      '#default_value' => $config->get('adroll_pix_id'),
      '#size' => 30,
      '#maxlength' => 30,
      '#required' => TRUE,
    ];
    $form['adroll_email'] = [
      '#type' => 'textfield',
      '#title' => t('AdRoll Email'),
      '#default_value' => $config->get('adroll_email'),
      '#size' => 30,
      '#maxlength' => 30,
    ];
    $form['roles'] = [
      '#type' => 'fieldset',
      '#title' => t('User role tracking'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#description' => t('Define what user roles should be tracked by AdRoll. Leave empty to track for all roles.'),
    ];
    $form['roles']['adroll_target_roles'] = [
      '#type' => 'checkboxes',
      '#options' => user_role_names(),
      '#default_value' => $config->get('adroll_target_roles'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('adroll.settings')
      ->set('adroll_adv_id', $form_state->getValue('adroll_adv_id'))
      ->set('adroll_pix_id', $form_state->getValue('adroll_pix_id'))
      ->set('adroll_email', $form_state->getValue('adroll_email'))
      ->set('adroll_target_roles', $form_state->getValue('adroll_target_roles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
